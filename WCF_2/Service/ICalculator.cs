﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Service
{
    [ServiceContract]
    public interface ICalculator
    {
        [OperationContract]
        [FaultContract(typeof(ForbidenAdd))]
        int Add(int a, int b);
    }

    [DataContract]
    public class ForbidenAdd
    {
        [DataMember]
        public string Message { get; set; }
    }
}
