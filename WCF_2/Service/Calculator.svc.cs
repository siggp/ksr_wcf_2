﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Service
{
    public class Calculator : ICalculator
    {
        public int Add(int a, int b)
        {
            var result = a + b;
            if (result == 41)
            {
                var fault = new ForbidenAdd();
                fault.Message = "Sory, Forbiden!";
                throw new FaultException<ForbidenAdd>(fault);
            } else
            {
                System.Threading.Thread.Sleep(3000);
                return result;
            }
        }
    }
}
