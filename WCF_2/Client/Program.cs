﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Client
{
    class Program
    {
        static int Main(string[] args)
        {
            var calc = new Calculator.CalculatorClient();
            var asyncResult = calc.BeginAdd(3, 4, Callback, calc);
            for (int i = 0; i < 10000000; i++)
            {
                Console.WriteLine("Dadada0");
            }
            return 0;
        }

        static void Callback(IAsyncResult res)
        {
            var calc = (Calculator.CalculatorClient)res.AsyncState;
            MessageBox.Show(calc.EndAdd(res).ToString());
        }

    }
}
